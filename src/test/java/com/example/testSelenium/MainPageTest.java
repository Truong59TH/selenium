package com.example.testSelenium;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class MainPageTest {
    MainPage mainPage = new MainPage();

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1280x800";
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeEach
    public void setUp() {
        open("https://www.phptravels.net/");
    }

    @Test
    public void shouldAnswerWidthTrue() {
        System.out.println("Hello");
        assertTrue(true);
    }

    @Test
    public void signUp() {
        $(".dropdown-login").click();
        $x("//a[@href='https://www.phptravels.net/register']").click();
        $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
        $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
        $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
        $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161@gmail.com");
        $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
        $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
//        $x("//button[@type='submit']").click();
    }

    @Test
    public void login() {
        $(".dropdown-login").click();
        $x("//a[@href='https://www.phptravels.net/login']").click();
        $x("//input[@type='email' and @name='username']").sendKeys("huynhtin13");
        $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
//        $x("//button[@type='submit']").click();
    }

    @Test
    public void searchDestination() {
//        mainPage.searchButton.click();

        $(".typeahead__container").click();
        $(".select2-focused").sendKeys("Hồ Chí Minh");
        $x("//span[@class='select2-match' and text()='Ho Chi Minh']").click();
        $x("//button[@type='submit']").click();
//        $x("//button[@type='submit' and text()='Search']").click();
//        $(".js-search-input").shouldHave(attribute("value", "Selenium"));
    }

//    @Test
//    public void toolsMenu() {
//        mainPage.toolsMenu.hover();
//
//        $x("//a[@class='sf-with-ul' and text()='Women']").shouldBe(visible);
//    }

//    @Test
//    public void navigationToAllTools() {
//        mainPage.seeAllToolsButton.click();
//
//        $(".products-list").shouldBe(visible);
//
//        assertEquals("All Developer Tools and Products by JetBrains", Selenide.title());
//    }

    @AfterAll
    public static void Finish() {
        System.out.println("Kết thúc");
    }
}
